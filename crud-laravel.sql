-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 18, 2024 at 04:28 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud-laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`id`, `name`, `phone_number`, `email`, `created_at`, `updated_at`) VALUES
(17, 'ssppp', '8780811187', 'hardik@logisticinfotech.co.in', '2023-10-28 22:23:11', '2023-10-28 22:59:03'),
(22, 'yu333', '7070707075', 'aagt@gmail.com', '2023-10-28 23:24:22', '2023-10-28 23:24:40'),
(23, 'ss', '563213224158', 'harzzzzdik@gmail.com', '2023-10-29 00:32:25', '2023-10-29 00:32:25'),
(25, 'd', '565+5+5+', 'adminaa@admin.com', '2024-04-18 05:17:46', '2024-04-18 05:17:46');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_10_27_172532_create_details_table', 1),
(6, '2023_10_29_060745_create_products_table', 2),
(7, '2023_10_29_075658_create_posts_table', 3),
(8, '2023_10_29_082308_create_students_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` longtext DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `detail`, `created_at`, `updated_at`) VALUES
(7, 'v c', 'cv', '2023-10-29 01:05:10', '2023-10-29 01:05:10'),
(8, 'vvbb', 'xxx', '2023-10-29 01:05:18', '2023-10-29 01:05:18'),
(9, 'sz', 'sz', '2023-10-29 02:13:06', '2023-10-29 02:13:06'),
(10, 'xx', 'xxsss', '2023-10-29 02:43:33', '2023-10-29 02:48:55');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `course`, `section`, `email`, `created_at`, `updated_at`) VALUES
(7, 'Ellie Jerde', 'BSC', 'iste', 'oziemann@rath.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(8, 'Alia Collins II', 'BSC', 'numquam', 'rluettgen@gmail.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(9, 'Carey Friesen', 'BSC', 'et', 'alexanne72@gmail.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(10, 'Dr. Cletus Crist', 'BSC', 'hic', 'lloyd.berge@crooks.biz', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(11, 'Mr. Rudolph Christiansen DDS', 'BSC', 'fugit', 'sroberts@morissette.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(12, 'Theo Buckridge', 'BSC', 'ut', 'alayna.hammes@ziemann.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(13, 'Teagan Kiehn V', 'BSC', 'et', 'treutel.ashley@gmail.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(14, 'Hulda Dietrich', 'BSC', 'tempore', 'rmuller@rodriguez.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(15, 'Cleveland Rogahn', 'BSC', 'sapiente', 'schmeler.victoria@denesik.info', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(16, 'Cristina Lynch', 'BSC', 'consequatur', 'lisandro86@runte.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(17, 'Julius Daugherty', 'BSC', 'voluptas', 'zora.gottlieb@lindgren.info', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(18, 'Mary Fadel', 'BSC', 'cupiditate', 'santino90@hotmail.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(19, 'Lula Stroman', 'BSC', 'ab', 'lehner.dawn@reichel.com', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(20, 'Prof. Felipe Kuhlman', 'BSC', 'voluptatum', 'allison43@bartoletti.biz', '2023-10-29 05:53:46', '2023-10-29 05:53:46'),
(21, 'Mr. Thurman Towne Jr.', 'BSC', 'quod', 'loren.torp@schaefer.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(22, 'Dr. Timothy Gerhold II', 'BSC', 'sint', 'erdman.josue@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(23, 'Nikki Nader', 'BSC', 'dolorem', 'amanda.sawayn@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(24, 'Anastasia Gottlieb', 'BSC', 'facilis', 'alec03@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(25, 'Ms. Molly Kassulke', 'BSC', 'dolores', 'dibbert.leslie@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(26, 'Mr. Jeremy Jast IV', 'BSC', 'dolore', 'alva98@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(27, 'Dr. Lowell Waelchi', 'BSC', 'hic', 'schuster.jovan@blanda.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(28, 'Al McClure DDS', 'BSC', 'nam', 'reynolds.coty@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(29, 'Larissa Schaden', 'BSC', 'rerum', 'tracey07@kilback.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(30, 'Prof. Concepcion Hickle Jr.', 'BSC', 'quibusdam', 'glenda.torphy@windler.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(31, 'Deven Boyer', 'BSC', 'sit', 'kuhn.frances@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(32, 'Jazmin Fahey', 'BSC', 'porro', 'janet04@boehm.org', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(33, 'Ed Carter', 'BSC', 'ipsa', 'kevin.ohara@gibson.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(34, 'Francesco Kreiger', 'BSC', 'doloremque', 'dejah52@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(35, 'Mr. Omer West', 'BSC', 'atque', 'ybeatty@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(36, 'Emily Ferry', 'BSC', 'ut', 'bernita.kilback@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(37, 'Maia Zboncak', 'BSC', 'voluptatum', 'aschowalter@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(38, 'Dell Hills DDS', 'BSC', 'corporis', 'gstanton@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(39, 'Prof. Hudson Greenfelder MD', 'BSC', 'vero', 'dubuque.idell@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(40, 'Mr. Roger Hickle IV', 'BSC', 'qui', 'cwalsh@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(41, 'Tremaine Zboncak', 'BSC', 'impedit', 'tillman.klein@funk.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(42, 'Josianne Gibson', 'BSC', 'sed', 'xkshlerin@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(43, 'Mafalda Stracke MD', 'BSC', 'provident', 'rhea81@koepp.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(44, 'Dan Boyle II', 'BSC', 'quia', 'owilderman@treutel.net', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(45, 'Denis Wilderman Sr.', 'BSC', 'mollitia', 'kdubuque@kertzmann.info', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(46, 'Justen Cremin IV', 'BSC', 'occaecati', 'upowlowski@heller.biz', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(47, 'Davonte Hoeger', 'BSC', 'ipsum', 'dejah96@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(48, 'Abel Boyle MD', 'BSC', 'quo', 'kkerluke@haley.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(49, 'Elyssa Buckridge', 'BSC', 'aspernatur', 'wyman.moen@bauch.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(50, 'Domenic Bergnaum DVM', 'BSC', 'nulla', 'stanton.kristofer@langosh.net', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(51, 'Obie Stracke', 'BSC', 'hic', 'fvon@larkin.info', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(52, 'Mohammad Marks', 'BSC', 'assumenda', 'sweimann@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(53, 'Mr. Kade Hodkiewicz Jr.', 'BSC', 'rem', 'marquardt.chanel@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(54, 'Marta Funk', 'BSC', 'nesciunt', 'ladarius.koss@deckow.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(55, 'Viola Bartoletti', 'BSC', 'soluta', 'helga.sipes@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(56, 'Miss Ena Barrows V', 'BSC', 'pariatur', 'francisco53@powlowski.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(57, 'Orie Harris', 'BSC', 'expedita', 'breinger@gibson.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(58, 'Luz Kozey DVM', 'BSC', 'veniam', 'ernesto.pfannerstill@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(59, 'Antonette Padberg', 'BSC', 'vitae', 'idell.schneider@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(60, 'Evelyn Herman', 'BSC', 'quod', 'brooks10@rodriguez.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(61, 'Torrance Sauer', 'BSC', 'excepturi', 'zwyman@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(62, 'Reba Turner Jr.', 'BSC', 'quos', 'stuart.greenfelder@koss.info', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(63, 'Gladys Legros', 'BSC', 'saepe', 'jolie.dietrich@tremblay.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(64, 'Dr. Lucie Howell Sr.', 'BSC', 'harum', 'miracle11@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(65, 'Kailyn Streich Sr.', 'BSC', 'velit', 'alyson04@champlin.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(66, 'Odie Krajcik', 'BSC', 'in', 'joanne.marks@koss.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(67, 'Carey Kling', 'BSC', 'sunt', 'winston.hackett@ondricka.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(68, 'Mrs. Nannie Doyle', 'BSC', 'aperiam', 'senger.junior@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(69, 'Mohammed Dietrich', 'BSC', 'earum', 'lemke.alessandra@lowe.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(70, 'Mr. Einar Howe II', 'BSC', 'consequatur', 'breitenberg.kristopher@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(71, 'Yasmine Strosin', 'BSC', 'est', 'ylynch@schulist.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(72, 'Pauline Lehner Jr.', 'BSC', 'animi', 'khudson@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(73, 'Dr. Merl Barrows', 'BSC', 'assumenda', 'nlockman@klocko.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(74, 'Newton Schinner I', 'BSC', 'officiis', 'tierra53@hahn.org', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(75, 'Lavina Gusikowski', 'BSC', 'id', 'gusikowski.ashleigh@mosciski.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(76, 'Courtney Kemmer', 'BSC', 'blanditiis', 'kassulke.jermain@hamill.net', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(77, 'Dr. Leonel Waelchi PhD', 'BSC', 'quibusdam', 'levi.walter@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(78, 'Miss Kimberly Connelly MD', 'BSC', 'nisi', 'sally03@wuckert.net', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(79, 'Mrs. Kianna Grant', 'BSC', 'aut', 'runolfsdottir.osbaldo@bogan.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(80, 'Jennyfer Schneider', 'BSC', 'velit', 'bmacejkovic@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(81, 'Hope Moen', 'BSC', 'non', 'vita.gerlach@renner.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(82, 'Leola Kessler', 'BSC', 'pariatur', 'heaven.wuckert@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(83, 'Dr. Evans Glover Sr.', 'BSC', 'autem', 'annabell.dicki@howell.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(84, 'Dr. Dylan Douglas', 'BSC', 'quaerat', 'coralie30@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(85, 'Verdie Jacobson', 'BSC', 'ut', 'fahey.elmo@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(86, 'Robyn Hoeger', 'BSC', 'esse', 'ystrosin@rolfson.biz', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(87, 'Mr. Carleton Satterfield II', 'BSC', 'accusantium', 'onolan@swaniawski.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(88, 'Dr. Donavon Ferry PhD', 'BSC', 'eius', 'xbeahan@zemlak.net', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(89, 'Rodger Doyle', 'BSC', 'id', 'jarvis91@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(90, 'Miller Rogahn MD', 'BSC', 'amet', 'adaline.wisozk@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(91, 'Pauline Hayes', 'BSC', 'animi', 'lesley.skiles@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(92, 'Teagan Lockman', 'BSC', 'et', 'adalberto.emard@harris.net', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(93, 'April Hagenes', 'BSC', 'fuga', 'juana.kreiger@hermiston.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(94, 'Kristy Denesik', 'BSC', 'cum', 'jacky.hermann@watsica.biz', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(95, 'Americo Keebler V', 'BSC', 'aut', 'eino75@leffler.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(96, 'Miss Diana Rodriguez DDS', 'BSC', 'dicta', 'norene.kuvalis@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(97, 'Gene Barrows', 'BSC', 'quos', 'orin.hagenes@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(98, 'Pete Von', 'BSC', 'ea', 'kgottlieb@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(99, 'Mr. Dylan Johns', 'BSC', 'autem', 'blangworth@stehr.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(100, 'Hellen Leannon', 'BSC', 'minus', 'gillian70@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(101, 'Amaya Olson', 'BSC', 'quasi', 'isobel.reichert@hamill.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(102, 'Donna Herman', 'BSC', 'minima', 'verla25@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(103, 'Estella Kuhn', 'BSC', 'aut', 'murphy88@gmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(104, 'Dr. Gilberto Grant III', 'BSC', 'est', 'willms.stuart@hotmail.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(105, 'Lela Windler', 'BSC', 'error', 'eduardo.howe@hane.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(106, 'Kayley Kuvalis', 'BSC', 'aliquam', 'xbalistreri@yahoo.com', '2023-10-29 05:53:47', '2023-10-29 05:53:47'),
(107, 'Jordi Wisozk', 'deleniti', 'molestiae', 'schamberger.aletha@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(108, 'Landen Collins Jr.', 'delectus', 'ut', 'micheal.legros@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(109, 'Vena Stroman PhD', 'aspernatur', 'illum', 'isac.mante@quigley.info', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(110, 'Jonathan Stracke', 'atque', 'eum', 'greenholt.zachary@rippin.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(111, 'Izabella Rohan', 'quasi', 'aut', 'everett.collier@monahan.info', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(112, 'Hollis Abbott', 'accusamus', 'qui', 'mclaughlin.delbert@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(113, 'Josianne Kulas', 'laborum', 'deserunt', 'hellen.smitham@hintz.biz', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(114, 'Ms. Makayla Prosacco', 'ipsa', 'deleniti', 'lconroy@wiza.info', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(115, 'Prof. Bryon Wilderman DVM', 'velit', 'veniam', 'xrohan@reichel.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(116, 'Prof. Kobe Collier I', 'quibusdam', 'voluptatum', 'ortiz.addie@olson.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(117, 'Holden Weissnat V', 'ad', 'velit', 'oullrich@okeefe.biz', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(118, 'Miss Emmanuelle Herzog', 'numquam', 'non', 'rylee13@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(119, 'Keira Schmidt', 'facere', 'voluptatem', 'johns.london@welch.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(120, 'Kristy Ferry', 'molestiae', 'suscipit', 'anabelle57@kunde.info', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(121, 'Jovany Marvin', 'omnis', 'maxime', 'erdman.paxton@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(122, 'Henry Metz PhD', 'qui', 'in', 'akirlin@feest.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(123, 'Winifred DuBuque', 'eum', 'asperiores', 'cmurazik@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(124, 'Mrs. Anika Keeling I', 'aut', 'ratione', 'merle75@oberbrunner.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(125, 'Darrick Oberbrunner', 'cumque', 'veniam', 'karson35@cole.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(126, 'Bernard Senger', 'inventore', 'maiores', 'oziemann@ruecker.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(127, 'Johnpaul Hansen', 'dolor', 'debitis', 'jreynolds@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(128, 'Justice Fahey', 'accusamus', 'occaecati', 'ansley.considine@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(129, 'Roderick Halvorson', 'quibusdam', 'facilis', 'bergstrom.dasia@hoppe.info', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(130, 'Bella Stracke', 'sit', 'nihil', 'sfadel@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(131, 'Ashleigh Cormier', 'sunt', 'ipsum', 'flangosh@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(132, 'Dr. Baylee Spencer', 'quas', 'architecto', 'nina.klocko@orn.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(133, 'Ms. Palma Simonis Jr.', 'voluptatem', 'nobis', 'herzog.catharine@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(134, 'Ahmad Grady', 'adipisci', 'et', 'roberta.gibson@price.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(135, 'Kaylah Wilderman', 'possimus', 'aliquid', 'sbrakus@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(136, 'Prof. Valentina Harvey I', 'consequatur', 'consequatur', 'omckenzie@hettinger.org', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(137, 'Wendy Bergnaum', 'quas', 'tenetur', 'zemlak.gregoria@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(138, 'Dr. Arno Feil DDS', 'aut', 'iusto', 'mike37@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(139, 'Verda Shanahan I', 'quia', 'amet', 'phansen@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(140, 'Amelie Wiza', 'perferendis', 'nihil', 'mmurazik@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(141, 'Rebecca Muller', 'iure', 'dolor', 'micah87@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(142, 'Melody Ryan', 'blanditiis', 'ut', 'efren88@predovic.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(143, 'Mr. Fabian Gleason', 'ut', 'nesciunt', 'akuhn@crist.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(144, 'Dr. Chance Corwin', 'impedit', 'vel', 'jackeline07@koepp.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(145, 'Mrs. Stacey Schaefer', 'nisi', 'dolor', 'wolff.myah@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(146, 'Dr. Dashawn Parker', 'voluptates', 'sequi', 'jwillms@okuneva.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(147, 'Emmet Roob', 'enim', 'nulla', 'osimonis@herman.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(148, 'Pasquale Hudson', 'quidem', 'exercitationem', 'jarret.shanahan@weissnat.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(149, 'Miss Mayra Smitham', 'vel', 'nisi', 'watsica.grace@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(150, 'Casimir Nienow', 'saepe', 'est', 'tbeier@weber.info', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(151, 'Dr. Grant Schultz', 'beatae', 'soluta', 'dillan.tremblay@steuber.info', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(152, 'Geoffrey Fahey', 'vel', 'consequatur', 'schumm.eveline@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(153, 'Mrs. Edythe Russel', 'consequatur', 'dolor', 'rosella.hammes@dibbert.org', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(154, 'General Zemlak', 'accusantium', 'magni', 'emmalee64@lindgren.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(155, 'Mr. Randy McDermott Sr.', 'ducimus', 'et', 'haag.assunta@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(156, 'Prof. Kody Kling', 'ab', 'qui', 'conrad88@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(157, 'Kailey Hills', 'eligendi', 'provident', 'mwilkinson@roob.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(158, 'Dr. Linda Stehr', 'excepturi', 'quibusdam', 'isaiah.bode@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(159, 'Greg Reynolds IV', 'vel', 'commodi', 'igottlieb@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(160, 'Giovanni Tillman', 'nihil', 'magni', 'kasandra64@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(161, 'Alexandra Dare', 'reprehenderit', 'et', 'zrogahn@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(162, 'Holly Maggio', 'voluptatem', 'explicabo', 'frank.grant@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(163, 'Marco Mann MD', 'assumenda', 'ex', 'webster.reichert@murazik.org', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(164, 'Silas Rice', 'reiciendis', 'quaerat', 'kirlin.kaylah@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(165, 'Demarcus Gaylord', 'odio', 'praesentium', 'rubie09@davis.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(166, 'Casimir Buckridge', 'id', 'eum', 'kling.miguel@kerluke.biz', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(167, 'Jackson Effertz Sr.', 'molestiae', 'qui', 'austyn14@nicolas.net', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(168, 'Christop Padberg', 'occaecati', 'rem', 'edna45@erdman.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(169, 'Rory Lynch II', 'sed', 'mollitia', 'gudrun39@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(170, 'Mona Franecki', 'praesentium', 'sint', 'marquardt.abel@roberts.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(171, 'Kristopher Deckow DDS', 'consequatur', 'expedita', 'maximillian.beatty@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(172, 'Judd Lind Jr.', 'inventore', 'officia', 'wilburn.luettgen@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(173, 'Ocie Bosco', 'vero', 'corporis', 'shields.freida@beatty.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(174, 'Silas Nicolas', 'minus', 'quis', 'ward.bell@denesik.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(175, 'Whitney Towne II', 'id', 'et', 'lambert.davis@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(176, 'Dr. Karine Shields', 'eius', 'officia', 'bkuhn@lebsack.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(177, 'Alyson Feeney', 'delectus', 'rerum', 'cecelia.hermann@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(178, 'Mac Morissette', 'suscipit', 'laborum', 'franecki.ciara@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(179, 'Aniya Abernathy', 'aspernatur', 'totam', 'gulgowski.london@botsford.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(180, 'Iliana Cartwright', 'quas', 'alias', 'block.zita@goyette.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(181, 'Emelia White Jr.', 'quis', 'esse', 'wgoodwin@abshire.org', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(182, 'Mr. Vinnie Donnelly', 'sed', 'quia', 'golda16@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(183, 'Gertrude Keeling', 'et', 'veniam', 'oconner.justyn@goodwin.org', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(184, 'Orin White', 'aperiam', 'fugit', 'savanna39@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(185, 'Ms. Elena Rutherford', 'quae', 'hic', 'garnet.will@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(186, 'Johnnie Runolfsson II', 'expedita', 'adipisci', 'kaleb.jast@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(187, 'Prof. Horacio Lynch', 'iure', 'tempore', 'erowe@pollich.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(188, 'Lenora Kilback', 'quas', 'nostrum', 'tressie10@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(189, 'Augustine Marks', 'voluptatibus', 'cum', 'buckridge.ricardo@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(190, 'Prof. Elfrieda Kulas V', 'sed', 'quia', 'cremin.leopold@hickle.net', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(191, 'Reva Lebsack I', 'doloremque', 'optio', 'ngrady@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(192, 'Mrs. Pansy Brekke', 'quia', 'libero', 'maxine28@medhurst.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(193, 'Edgar Funk', 'assumenda', 'ut', 'jaron56@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(194, 'Doug Grant', 'exercitationem', 'est', 'eturcotte@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(195, 'Asha Johns Jr.', 'velit', 'accusantium', 'emely27@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(196, 'Mrs. Liza Armstrong', 'sapiente', 'in', 'josiane57@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(197, 'Prof. Loma Barton', 'similique', 'occaecati', 'qweissnat@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(198, 'Tyrique Kutch', 'voluptatibus', 'illum', 'zita43@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(199, 'Moses Stanton', 'magnam', 'voluptate', 'kelvin98@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(200, 'Melyna O\'Hara', 'dolores', 'doloremque', 'berta20@farrell.biz', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(201, 'Carlee Russel', 'voluptate', 'quos', 'marcia62@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(202, 'Armand Kilback', 'recusandae', 'non', 'jefferey.jast@koelpin.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(203, 'Mr. Reynold Herzog III', 'accusantium', 'perspiciatis', 'tkreiger@douglas.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(204, 'Jake Crooks', 'eos', 'est', 'olin.herman@hotmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(205, 'Gwen Volkman', 'magnam', 'cupiditate', 'egaylord@gmail.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(206, 'Ottis Rogahn MD', 'facilis', 'et', 'tanya53@yahoo.com', '2023-10-29 05:54:11', '2023-10-29 05:54:11'),
(207, 'ssdd', 'BAD', 'ss', 'aaa@gmail.comm', '2024-04-09 05:31:01', '2024-04-09 05:31:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `details_email_unique` (`email`),
  ADD UNIQUE KEY `details_phone_number_unique` (`phone_number`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
